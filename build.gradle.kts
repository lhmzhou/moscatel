import org.springframework.cloud.contract.verifier.config.TestFramework
import org.springframework.cloud.contract.verifier.plugin.ContractVerifierExtension
group = "com.demo"

repositories {
    mavenLocal()
    maven {
        url = uri("")
    }
}

plugins {
    kotlin("jvm") version "1.3.21"
    kotlin("plugin.spring") version "1.3.21"
    kotlin("plugin.jpa") version "1.3.21"
    id("org.liquibase.gradle") version "2.0.1"
    id("org.springframework.boot") version "2.2.1.RELEASE"
    id("org.jlleitschuh.gradle.ktlint") version "7.3.0"
    id("net.linguica.maven-settings") version "0.5"
    id("org.springframework.cloud.contract") version "2.1.1.RELEASE"
    jacoco
    `maven-publish`
    id("com.adarshr.test-logger") version "1.6.0"
    id("net.idlestate.gradle-duplicate-classes-check") version "1.0.2"
    id("com.github.ben-manes.versions") version "0.21.0"
    id("com.gorylenko.gradle-git-properties") version "2.0.0"
    id("com.palantir.docker") version "0.22.0"
}

apply(plugin = "io.spring.dependency-management")
apply(plugin = "kotlin-allopen")
apply(plugin = "org.liquibase.gradle")

allOpen {
    annotation("javax.persistence.Entity")
}

val springCloudVersion = "2.1.1.RELEASE"
val cbpCloudConfigClientVersion = "1.0.0"
val cbpSpringServiceCommonVersion = "1.0.2"
val cbpAvroCommonVersion = "0.0.3"
val cbpAvroFundingVersion = "0.0.6"
val identityClientVersion = "1.13.0"
val kotlinLoggingVersion = "1.6.26"
val swaggerVersion = "2.9.2"
val oracleDriverVersion = "12.2.0.1.0"
val bootVersion = "2.2.1.RELEASE"
val cloudStarterVersion = "2.1.2.RELEASE"
val cloudStreamVersion = "3.0.0.RELEASE"
val confluentVersion = "5.3.1"
val avroVersion = "1.9.0"
val elasticVersion = "1.1.4"
val jaegerCloudStarterVersion = "2.0.3"
val kafkaSecureSerializersVersion = "2.6"
val settlementAvroVersion = "0.0.1"

// TODO upgrading to 5.4.2 breaks tests with a classpath error
val junit5version = "5.3.2"
val kotlinTestVersion = "3.3.2"
val springMockKVersion = "1.1.1"
val mockkVersion = "1.9.3"
val wiremockVersion = "2.22.0"

val h2Version = "1.4.199"
val liquibaseCoreVersion = "3.6.3"
val liquibaseGradlePluginVersion = "2.0.1"
val liquibaseGroovyDslVersion = "2.0.2"
val logbackVersion = "1.2.3"
val snakeYamlVersion = "1.24"

val cbpAvroGatewayVersion = "0.0.1"

// Test specific
val kafkaTestVersion = "2.2.5.RELEASE"

val jacocoCoverageExclusions = listOfNotNull(
        "**/FundingLimitApplication*",
        "**/KafkaConfig*"
)

dependencies {
    implementation(kotlin("stdlib-jdk8"))
    implementation(kotlin("reflect"))

    implementation("com.demo.cbp", "spring-service-common", cbpSpringServiceCommonVersion)
    implementation("com.demo.cbp", "cloud-config-client", cbpCloudConfigClientVersion)
    implementation("com.demo.cbp.avro", "common", cbpAvroCommonVersion)
    implementation("com.demo.cbp.avro", "funding", cbpAvroFundingVersion)
    implementation("com.demo.cbp.avro", "gateway", cbpAvroGatewayVersion)
    implementation("com.demo.cbp.avro", "settlement", settlementAvroVersion)
    implementation("com.demo.security.identityclient", "identityclient_filter", identityClientVersion)
    implementation("net.sf.ehcache", "ehcache", "2.7.1")

    implementation("net.javacrumbs.shedlock", "shedlock-spring", "3.0.0")

    implementation("org.springframework.boot", "spring-boot-starter-web")
    implementation("org.springframework.boot", "spring-boot-starter-actuator")
    implementation("org.springframework.boot", "spring-boot-starter-data-jpa")
    implementation("org.springframework.cloud", "spring-cloud-starter-config", cloudStarterVersion)
    implementation("com.fasterxml.jackson.module", "jackson-module-kotlin")
    implementation("com.h2database", "h2")
    implementation("oracle", "ojdbc8", oracleDriverVersion)
    implementation("org.hibernate", "hibernate-envers")
    implementation("com.google.code.gson", "gson")
    implementation("io.github.microutils", "kotlin-logging", kotlinLoggingVersion)
    implementation("io.springfox", "springfox-swagger2", swaggerVersion)
    implementation("io.springfox", "springfox-swagger-ui", swaggerVersion)
    implementation("org.springframework.cloud", "spring-cloud-starter-stream-kafka", cloudStreamVersion)
    implementation("org.springframework.cloud", "spring-cloud-stream-binder-kafka", cloudStreamVersion)
    implementation("org.springframework.cloud", "spring-cloud-stream-schema", "2.1.3.RELEASE")
    implementation("org.apache.avro", "avro", avroVersion)
    implementation("io.confluent", "kafka-avro-serializer", confluentVersion)
    implementation("io.confluent", "kafka-schema-registry-client", confluentVersion)
    implementation("io.micrometer", "micrometer-registry-elastic", elasticVersion)
    implementation("com.demo.platform.sec.kafka", "kafka-secure-serializers", kafkaSecureSerializersVersion)
    implementation("io.opentracing.contrib", "opentracing-spring-jaeger-cloud-starter", jaegerCloudStarterVersion)
    implementation("oracle", "ojdbc8", oracleDriverVersion)
    compile("org.liquibase", "liquibase-core", liquibaseCoreVersion)
    compile("org.liquibase", "liquibase-gradle-plugin", liquibaseGradlePluginVersion)

    liquibaseRuntime("org.liquibase", "liquibase-gradle-plugin", liquibaseGradlePluginVersion)
    liquibaseRuntime("org.liquibase", "liquibase-groovy-dsl", liquibaseGroovyDslVersion)
    liquibaseRuntime("org.liquibase", "liquibase-core", liquibaseCoreVersion)
    liquibaseRuntime("ch.qos.logback", "logback-core", logbackVersion)
    liquibaseRuntime("ch.qos.logback", "logback-classic", logbackVersion)
    liquibaseRuntime("com.h2database", "h2", h2Version)
    liquibaseRuntime("org.yaml", "snakeyaml", snakeYamlVersion)
    liquibaseRuntime("oracle", "ojdbc8", oracleDriverVersion)

    testRuntimeOnly("org.junit.jupiter", "junit-jupiter-engine")
    testImplementation("org.jetbrains.kotlin", "kotlin-test", "1.3.41")
    testImplementation("org.junit.jupiter", "junit-jupiter-api")
    testImplementation("io.kotlintest", "kotlintest-runner-junit5", kotlinTestVersion)
    testImplementation("io.kotlintest", "kotlintest-extensions-spring", kotlinTestVersion)
    testImplementation("io.mockk", "mockk", mockkVersion)
    testImplementation("com.ninja-squad", "springmockk", springMockKVersion)
    testImplementation("com.github.tomakehurst", "wiremock", wiremockVersion)
    testImplementation("org.springframework.boot", "spring-boot-starter-test") {
        exclude(module = "junit") // For JUnit 5 instead of 4
        exclude(module = "mockito-core") // For use of MockK instead
    }

    testImplementation("org.springframework.cloud", "spring-cloud-stream-test-support", cloudStreamVersion)
    testImplementation("org.springframework.kafka", "spring-kafka-test", kafkaTestVersion)

    runtime("org.springframework.boot", "spring-boot-devtools", bootVersion)
    testCompile(kotlin("test"))
    testImplementation("org.springframework.cloud", "spring-cloud-starter-contract-verifier", springCloudVersion)
    testImplementation("org.springframework.cloud", "spring-cloud-starter-contract-stub-runner", springCloudVersion)
}

configurations.all {
    exclude("org.slf4j", "slf4j-log4j12")
    exclude("org.slf4j", "slf4j-nop")
}

configure<ContractVerifierExtension> {
    packageWithBaseClasses = "com.demo.cbp.funding.limit.contract"
    stubsOutputDir = file("${project.buildDir}/production/${project.name}-stubs/")
    testFramework = TestFramework.JUNIT5
}
tasks {
    compileKotlin {
        kotlinOptions {
            jvmTarget = "1.8"
            // Enables strict null safety
            freeCompilerArgs = listOfNotNull("-Xjsr305=strict")
        }
    }

    compileTestKotlin {
        kotlinOptions {
            jvmTarget = "1.8"
            // Enables strict null safety
            freeCompilerArgs = listOfNotNull("-Xjsr305=strict")
        }
    }

    // Customize the git properties shown in the /api/info endpoint, see https://github.com/n0mer/gradle-git-properties
    gitProperties {
        keys =
                listOf("git.branch", "git.commit.id", "git.commit.message.short", "git.commit.user.name", "git.commit.time")
    }

    springBoot {
        buildInfo {
            properties {
                additional = mapOf("build_number" to (System.getenv("env.BUILD_NUMBER") ?: "-Unknown-"))
            }
        }
    }

    bootRun {
        jvmArgs(listOf("-Dspring.profiles.active=e0,kafka"))
    }

    test {
        setExcludes(listOf("**/*IntegrationTest*.*"))
        useJUnitPlatform {}
        // Log out test results (may not be needed)
        testLogging {
            events("passed", "skipped", "failed")
        }
    }

    val integrationTest by creating(Test::class) {
        description = "Runs integration tests."
        group = "verification"

        useJUnitPlatform()
        // Log out test results (may not be needed)
        testLogging {
            events("passed", "skipped", "failed")
        }
        setIncludes(listOf("**/*IntegrationTest*.*"))
        shouldRunAfter("test")
    }

    val jacocoTestCoverageVerification by getting(JacocoCoverageVerification::class) {
        mustRunAfter("jacocoTestReport")
        violationRules {
            rule {
                limit {
                    counter = "LINE"
                    minimum = "0.93".toBigDecimal()
                }
            }
        }
        afterEvaluate {
            classDirectories.setFrom(files(classDirectories.files.map {
                fileTree(it) {
                    exclude(jacocoCoverageExclusions)
                }
            }))
        }
    }

    val jacocoTestReport by getting(JacocoReport::class) {
        afterEvaluate {
            classDirectories.setFrom(files(classDirectories.files.map {
                fileTree(it) {
                    exclude(jacocoCoverageExclusions)
                }
            }))
        }
    }

    dependencyUpdates {
        resolutionStrategy {
            componentSelection {
                all {
                    val rejected =
                        listOf("alpha", "beta", "rc", "cr", "m", "preview", "b", "ea", "dev").any { qualifier ->
                            candidate.version.matches(Regex("(?i).*[.-]$qualifier[.\\d-+]*"))
                        }
                    if (rejected) {
                        reject("Release candidate")
                    }
                }
            }
        }
    }

    checkForDuplicateClasses {
        // TODO ignoring here, because the dupes check is far too course grained - it finds dupes in Jacoco!
        ignoreFailures = true
    }

    check {
        dependsOn(integrationTest)
        dependsOn("jacocoTestReport")
        dependsOn("jacocoTestCoverageVerification")
    }

    val copyJar by creating(Copy::class) {
        into(project.buildDir)
        into("appBundle") {
            from(bootJar.get().archiveFile.get())
            rename { "app.jar" }
        }
        into("appBundle/.openshift") {
            from("etc/paas/openshift")
        }
    }

    val tarGz by creating(Tar::class) {
        dependsOn(copyJar)
        from("${project.buildDir}/appBundle")
        compression = Compression.GZIP
        destinationDirectory.set(project.buildDir)
        archiveBaseName.set(rootProject.name)
        archiveFileName.set(bootJar.get().archiveFile.get().asFile.name.replace(".jar", ".tar.gz"))
    }

    publish {
        dependsOn(tarGz)
    }

    docker {
        name = "docker.demo.com/fxip/funding-limit"
        files(bootJar.get().archiveFile.get().asFile)
        buildArgs(mapOf("JAR_FILE" to bootJar.get().archiveFile.get().asFile.name))
    }
}

publishing {
    publications {
        create<MavenPublication>("archive") {
            artifact(tasks["tarGz"]) {
                extension = "tar.gz"
            }
        }
    }
}

liquibase {
    "mainClassName" to "org.liquibase.gradle.OutputEnablingLiquibaseRunner"
    activities.register("updateSQL") {
        this.arguments = mapOf(
                "logLevel" to "debug",
                "changeLogFile" to "src/main/resources/db/changelog-master.yaml",
                "url" to findProperty("url"),
                "driver" to findProperty("driver"),
                "username" to findProperty("username"),
                "password" to findProperty("password")
        )
    }
    activities.register("update") {
        this.arguments = mapOf(
                "logLevel" to "debug",
                "changeLogFile" to "src/main/resources/db/changelog-master.yaml",
                "url" to findProperty("url"),
                "driver" to findProperty("driver"),
                "username" to findProperty("username"),
                "password" to findProperty("password")
        )
    }
    activities.register("tag") {
        this.arguments = mapOf(
                "logLevel" to "debug",
                "changeLogFile" to "src/main/resources/db/changelog-master.yaml",
                "url" to findProperty("url"),
                "driver" to findProperty("driver"),
                "username" to findProperty("username"),
                "password" to findProperty("password")
        )
    }
    runList = findProperty("runList")
}
