# moscatel

## Purpose 
To demonstrate how we can produce messages to Kafka, using an avro schema to serialize/deserialize complex event objects

`moscatel` demos 1. Creation of event object, 2. Sending to Kafka, 3. Consuming from Kafka, and 4. Some logging of message fields for validating the setup

# How to use

Clone and run a gradle build:

```gradle clean build```

To send a message: http://localhost:8080/sendmessage?messageId=<message_id>

This will construct a kafka event object with the required header and fields, with `message_id` in the Name field. This is logged on the way in and out to prove the behaviour.

