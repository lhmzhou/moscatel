package kotlin.demo

import kotlin.demo.cbp.config.PollingSpringCloudConfigClientConfig
import kotlin.demo.cbp.springservicecommon.config.CbpApiCommonConfig
import mu.KotlinLogging
import org.springframework.beans.factory.annotation.Value
import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication
import org.springframework.cloud.stream.schema.client.ConfluentSchemaRegistryClient
import org.springframework.cloud.stream.schema.client.EnableSchemaRegistryClient
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.context.annotation.Import
import java.net.InetAddress

private const val SYSPROP_PROFILES_ACTIVE = "spring.profiles.active"
private val logger = KotlinLogging.logger {}

@SpringBootApplication
@Import(CbpApiCommonConfig::class, PollingSpringCloudConfigClientConfig::class)
class KafkaKotlinProducerApplication

fun main(args: Array<String>) {
    logger.info("Start in main application ${KafkaKotlinProducerApplication::class} on host ${InetAddress.getLocalHost().hostAddress}")

    requireNotNull(System.getProperty(SYSPROP_PROFILES_ACTIVE)) { "$SYSPROP_PROFILES_ACTIVE sysprop missing, e.g. e1" }
    runApplication<KafkaKotlinProducerApplication>(*args)
}

@Configuration
@EnableSchemaRegistryClient
class KafkaConfig {
    @Bean
    fun schemaRegistryClient(@Value("\${spring.cloud.stream.schemaRegistryClient.endpoint}") endpoint: String) =
            ConfluentSchemaRegistryClient().apply { setEndpoint(endpoint) }
}
