package kotlin.demo.config

import io.jaegertracing.internal.JaegerTracer
import io.jaegertracing.internal.reporters.InMemoryReporter
import io.jaegertracing.internal.samplers.ConstSampler
import mu.KotlinLogging
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration

private val logger = KotlinLogging.logger { }

@ConditionalOnProperty(value = ["opentracing.jaeger.enabled"], havingValue = "false", matchIfMissing = false)
@Configuration
class TracerConfig {

    @Bean
    fun jaegerTracer(): io.opentracing.Tracer {
        logger.info("Tracing is disabled.")
        val reporter = InMemoryReporter()
        val sampler = ConstSampler(false)
        return JaegerTracer.Builder("untraced-service")
                .withReporter(reporter)
                .withSampler(sampler)
                .build()
    }
}
