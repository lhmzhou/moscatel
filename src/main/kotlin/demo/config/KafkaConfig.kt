package kotlin.demo.config

import org.springframework.cloud.stream.annotation.Input
import org.springframework.cloud.stream.annotation.Output
import org.springframework.messaging.MessageChannel
import org.springframework.messaging.SubscribableChannel

interface KafkaConfig {
    @Output("outgoingChannel")
    fun exampleOutputChannel(): MessageChannel

    @Input("exampleInputChannel")
    fun receiveExampleInputChannel(): SubscribableChannel
}
