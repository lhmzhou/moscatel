package kotlin.demo.consumer

import kotlin.demo.cbp.avro.gateway.GatewayMessage
import kotlin.demo.example.config.KafkaConfig
import mu.KotlinLogging
import org.springframework.cloud.stream.annotation.EnableBinding
import org.springframework.cloud.stream.annotation.StreamListener
import org.springframework.messaging.Message
import org.springframework.stereotype.Component

private val logger = KotlinLogging.logger { }

@Component
@EnableBinding(KafkaConfig::class)
class MessageConsumer {
    @StreamListener(target = "exampleInputChannel")
    fun consumeSettlementGatewayEvent(gatewayMessage: Message<GatewayMessage>) {
        logger.info { "Received GW Message ${gatewayMessage.payload}" }
    }
}
