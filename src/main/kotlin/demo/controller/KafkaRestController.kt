package kotlin.demo.controller

import mu.KotlinLogging

import org.springframework.cloud.stream.annotation.EnableBinding
import org.springframework.http.MediaType
import org.springframework.integration.support.MessageBuilder
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RequestParam
import org.springframework.web.bind.annotation.RestController

import kotlin.demo.config.KafkaConfig

import kotlin.demo.cbp.avro.common.Header
import kotlin.demo.cbp.avro.gateway.GatewayMessage
import kotlin.demo.cbp.avro.settlement.SettlementDelivered

import java.time.Instant
import java.time.ZonedDateTime
import java.util.UUID

private val logger = KotlinLogging.logger { }

@RestController
@EnableBinding(KafkaConfig::class)
class KafkaRestController(
    val producer: KafkaConfig
) {
    val domain = "Settlement"
    val service = "Settlement Processing"
    val name = "Settlement Delivered to Counterparty Event"
    val originatorId = "origin id"
    val counterpartyBankDeliveredTimestamp = ZonedDateTime.parse("2021-08-25T09:10:22Z").toString()
    val timestamp = Instant.now()
    @GetMapping(produces = [MediaType.APPLICATION_JSON_VALUE])
    fun testGet(): String {
        return "Hello!"
    }

    @GetMapping(path = ["/sendmessage"], produces = [MediaType.APPLICATION_JSON_VALUE])
    fun sendMessage(@RequestParam messageId: String): String {
        val payload = generateMessage(messageId)
        producer.exampleOutputChannel().send(MessageBuilder.withPayload(payload).build())
        logger.info("Received message via get request")
        logger.info("Sending message with " + producer.exampleOutputChannel().javaClass.name)
        logger.info("ID: ${payload.getHeader().getId()}")
        return "Message Sent $messageId"
    }

    @GetMapping(path = ["/sendgw"], produces = [MediaType.APPLICATION_JSON_VALUE])
    fun sendGWMessage(@RequestParam content: String, @RequestParam logicalQ: String): String {
        val payload = generateGWMessage(content, logicalQ)
        producer.exampleOutputChannel().send(MessageBuilder.withPayload(payload).build())
        logger.info("Received message via get request")
        logger.info("Sending message with " + producer.exampleOutputChannel().javaClass.name)
        logger.info("ID: ${payload.getHeader().getId()}")
        logger.info { "Message Sent to $logicalQ with content=$content" }
        return "Messages sent!"
    }

    fun generateMessage(messageId: String): SettlementDelivered {
        val uuid = UUID.randomUUID().toString()

        val header = Header.newBuilder()
            .setCorrelationId(uuid)
            .setId(uuid)
            .setName(name)
            .setDomain(domain)
            .setService(service)
            .setTimestamp(timestamp)
            .setOriginatorId(originatorId)
            .build()

        return SettlementDelivered.newBuilder()
            .setHeader(header)
            .setCounterpartyBankDeliveredTimestamp(counterpartyBankDeliveredTimestamp) 
            .setPaymentId(messageId) 
            .build()
    }

    fun generateGWMessage(content: String, eventName: String): GatewayMessage {
        val uuid = UUID.randomUUID().toString()

        val header = Header.newBuilder()
            .setCorrelationId(uuid)
            .setId(uuid)
            .setName(eventName)
            .setDomain(domain)
            .setService(service)
            .setTimestamp(timestamp)
            .setOriginatorId(originatorId)
            .build()

        return GatewayMessage.newBuilder()
            .setHeader(header)
            .setPayload(content)
            .build()
    }
}
